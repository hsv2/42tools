#!/bin/zsh
# vi: et sw=4 ts=4:
# hsv2

LOCAL_DIR="${2:="$HOME/goinfre/"}"
REMOTE_DIR="${3:="$HOME/sgoinfre/goinfre/"}"
ARCHIVE="${4:="$HOME/sgoinfre/goinfre.tar.gz"}"
MARKER=".sync.sh.marker"
LOCAL_MARKER="$LOCAL_DIR/$MARKER"
REMOTE_MARKER="$REMOTE_DIR/$MARKER"

if [[ ! ( -e "$LOCAL_DIR" && -e "$REMOTE_DIR") ]]
then
    echo "ABORTING: local or remote directory missing"
    exit 1
fi


if [[ ($1 == "in" || -z $1) && (! -e "$LOCAL_MARKER" || "$LOCAL_MARKER" -ot "$REMOTE_MARKER") ]]
then
    echo "sync in: $LOCAL_DIR"
    if [[ -f "$ARCHIVE" ]]
    then
        tar xzf "$ARCHIVE" -C "$LOCAL_DIR"
    fi
    rsync -a --progress "$REMOTE_DIR/" "$LOCAL_DIR"
elif [[ $1 == "out" && (! -e "$REMOTE_MARKER" || "$REMOTE_MARKER" -ot "$LOCAL_DIR") ]]
then
    echo "sync out: $REMOTE_DIR"
    echo "cleaning brew"
    brew autoremove
    brew cleanup --prune=all
	tar czf "$ARCHIVE.tmp" -C "$LOCAL_DIR" . || (echo "FATAL ERROR. EXITING"; exit 1)
	mv -v "$ARCHIVE.tmp" "$ARCHIVE"
    rsync -a --progress --delete-delay "$LOCAL_DIR/" "$REMOTE_DIR"
    touch "$REMOTE_MARKER"
elif [[ $1 != "in" && $1 != "out" ]]
then
    echo -e "parameter invalid\nsyntax: $0 {in|out} [ local [ remote ] ]"
    exit 1
fi

echo "Up to date"
